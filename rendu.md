# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Hammoudi, Mohamed-Said, email: mohamedsaid.hammoudi.etu@univ-lille.fr

- Hallil, Razine, email: razine.hallile.etu@univ-lille.fr

## Question 1

Réponse
* **create user** : sudo useradd toto
* **add to ubuntu groupe** : sudo adduser toto ubuntu
* **create file titi.txt** : touch titi.txt
* **change user id to toto** : sudo chown toto titi.txt
* **remove write permission for the user** : sudo chmod -w titi.txt
* **lunch titi.txt with toto user** : sudo -u toto vim titi.txt
On constate qu'en lençant le fichier avec l'utlisateur toto on ne peut pas écrire dedans parce qu'il a pas les droits d'écritures.  
Donc la réponse est non, le processus peut pas écrire.


## Question 2

Réponse
* x pour un répertoire : on peut accéder au répertoire.
* Création de repertoire mydir : mkdir mydir
* Enlever les droits d'exécution au groupe ubuntu : chmod g-x mydir
* Entrer dans toto : sudo -u toto -s
* cd mydir : bash: cd: mydir/: Permission denied
puisque toto apartient au groupe ubuntu et puisque on a enlevé les droits d'exécution du groupe, toto peut pas accéder dans le repertoire.

* Création du fichier data.txt : touch mydir/data.txt
* Changer d'utilisateur : sudo -u toto vim titi.txt
* Lister le contenu du répertoire : ls -al mydir
```
>>> -????????? ? ? ? ?            ? data.txt
```
On arrive à lister le contenu du repertoire mydir mais sans voir les permissions du contenu.

## Question 3

Réponse  
- Les valeurs des différents id en exécutant le programme avec l'utilisateur ubuntu est:     
  ![image](captures/exo3-1.PNG)

- Les valeurs des différents id en exécutant le programme avec l'utilisateur toto est:  
  ![image](captures/exo3-0.PNG)   
  Le processus n'arrive pas à ouvrir le fichier en lecture comme on le voit sur la capture précédente.

- Les valeurs des différents id en exécutant le programme avec l'utilisateur toto et après avoir activer le set-user-id est:  
  ![image](captures/exo3-2.PNG)    
  Le processus arrive à ouvrir le fichier(notre fichier est vide) en lecture comme on le voit sur la capture précédente.

## Question 4
Réponse  
- Les valeurs des différents id en exécutant le programme avec l'utilisateur ubuntu est:  
	![image](captures/exo4-01.PNG)  

- Les valeurs des différents id en exécutant le programme avec l'utilisateur toto et après avoir activer le set-user-id est:  
	![image](captures/exo4-02.png)  
	![image](captures/exo4-03.png)    

- On remarque que dans le script python, lors de son exécution la valeur du EUID obtenue n'est pas la même qu'avec la question précédente. 

## Question 5

- La commande "chfn" permet de modifier les informations personnel des utilisateurs contenu dans le fichier "/etc/passwd" (Son nom,
prénom, son numéro de téléphone,...).   
- Le résultat de ls -al /usr/bin/chfn est :  
  ![image](captures/exo-5-1.PNG)  
- Explications des permissions : Il n'y a que l'utilisateur root qui puisse effectuer des changements des informations personnels de tous les utilisateurs.    
- On visualise le fichier /etc/passwd avant de changer les informations de toto  
  ![image](captures/exo5-2.PNG)  
  Après l'exécution de la commande "chfn toto", les informations de l'utlilsateur toto ont bien été mis à jour comme on le voir sur cette photo  
  ![image](captures/exo5-3.png)  

## Question 6

Réponse

- Les mots de passe des utilisateurs sont stockés dans **/etc/shadow**, et sont chiffrés.
```
-rw-r----- 1 root shadow 1156 Jan 20 00:46 /etc/shadow
```
- Comme on peut le voir, seul l'utilisateur root peut lire et écrire dans ce fichier, sinon le fichier est chiffré pour tout les utilisateurs. Le groupe shadow peut aussi lire le fichier.  
![image](captures/shadow.png)


## Question 7

Mettre les scripts bash dans le repertoire *question7*.
$ sudo useradd lambda_a
$ sudo useradd lambda_b

$ sudo groupadd groupe_a
$ sudo groupadd groupe_b

$ sudo usermod -a -G groupe_a lambda_a
$ sudo usermod -a -G groupe_b lambda_b

$ sudo adduser admin --ingroup admin

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 









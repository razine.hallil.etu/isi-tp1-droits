import os
import sys

if __name__ =="__main__":
    
    euid = os.geteuid()
    egid = os.getegid()
    if len(sys.argv) < 2:
        print("Missing argument, please choose a file")
    print("EUID : ",euid)
    print("EGID : ",egid)
    
    try:
        f = open(sys.argv[1],'r')
        for line in f:
            print(line)
        print("File opens correctly")
        f.close()
    except OSError:
        print("Cannot open file")
        sys.exit()
    
        

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
int main(int argc, char *argv[])
{
    FILE *f;

    if (argc < 2)
    {
        /* code */
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }
    f = fopen(argv[1], "r");
    if (f == NULL)
    {
        /* code */
        perror("cannot open file");
        exit(EXIT_FAILURE);
    }
    printf("RUID : %d \n",getuid());
    printf("RGID : %d \n",getgid());
    printf("EUID : %d \n",geteuid());
    printf("EGID : %d \n",getegid());
    // Read contents from file 
    char c;
    c = fgetc(f); 
    while (c != EOF) 
    { 
        printf ("%c", c); 
        c = fgetc(f); 
    } 
    fclose(f);
    exit(EXIT_SUCCESS);
    
}
/*
RUID : id utilisateur --> getuid()
RGID : id du groupe principal auquel appartient l'utilisateur qui as lancé le processus. --> getgid()
EUID = RUID --> geteuid()
EGID = RGID --> getegid()

listes des groupes supplémentaires --> getgroups()
*/
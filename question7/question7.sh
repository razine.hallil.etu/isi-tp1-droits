#!/bin/bash

# Hammoudi Mohamed-said
# Hallil Razine 

echo "-------------*Serveur de fichiers partagés*-------------------"

user_a=lambda_a
user_b=lambda_b
admin=admin

group_a=groupe_a
group_b=groupe_b
group_c=admin

rep_a=dir_a
rep_b=dir_b
rep_c=dir_c

file_a=a.txt
file_b=b.txt
file_c=c.txt


echo "creation de l'utilisateur $user_a "
sudo useradd  $user_a

echo "creation du groupe $group_a "
sudo groupadd $group_a

echo "Ajout de $user_a dans le groupe $group_a "
sudo usermod -a -G $group_a $user_a



echo "creation de l'utilisateur $user_b "
sudo useradd $user_b

echo "creation du groupe $group_b \n"
sudo groupadd $group_b

echo "Ajout de $user_b dans le groupe $group_b "
sudo usermod -a -G $group_b $user_b

echo "admin"


echo "ajouter  $admin dans le groupe $group_c "
sudo adduser $admin --ingroup admin
# Mot de passe 
echo "admin"
echo "admin"

echo "ajouter  $user_a dans le groupe $group_c "
sudo adduser $user_a --ingroup $group_c

echo "ajouter  $user_b dans le groupe $group_c "
sudo adduser $user_b --ingroup $group_c

# creation des répértoire
echo "creation des repertoir $rep_a $rep_b $rep_c "
sudo mkdir $rep_a $rep_b $rep_c

#creation des fichiers dans les dossiers déja creer
echo "creation des fichiers $file_a $file_b $file_c \n "
sudo touch $rep_a/$file_a
sudo touch $rep_b/$file_b
sudo touch $rep_c/$file_c

sudo chown $USER $rep_a/$file_a
sudo chown $USER $rep_b/$file_b
sudo chown $USER $rep_c/$file_c





# Ajouter chaque groupe a son propre repertoir
sudo chgrp -R -v $group_a $rep_a
sudo chgrp -R -v $group_b $rep_b
sudo chgrp -R -v $group_c $rep_c

sudo echo "hello $rep_a" >> $rep_a/$file_a
sudo echo "hello $rep_b" >> $rep_b/$file_b
sudo echo "hello $rep_c" >> $rep_c/$file_c

echo "supprimer les permissions pour les autres groupes"
sudo chmod -R o-rwx $rep_a
sudo chmod -R o-rwx $rep_b
sudo chmod -R o-rwx $rep_c
sudo chmod -R o+r $rep_c

sudo chmod -R g+rwx $rep_a
sudo chmod -R g+rwx $rep_b
sudo chmod -R g+rwx $rep_c

sudo chown -R $admin $rep_c
sudo chown -R $admin $rep_a
sudo chown -R $admin $rep_b

sudo chmod g=r $rep_c

sudo chmod +t $rep_a
sudo chmod +t $rep_b


